FROM centos:centos6.9

ENV PHP_VERSION 5.2.17

RUN rpm --rebuilddb \
    && yum update -y \
    && yum groupinstall -y 'Development Tools' \
    && yum install -y epel-release

RUN rpm --rebuilddb && yum update -y && yum install -y \
    wget \
    curl curl-devel \
    git \
    bzip2 \
    tar \
    sendmail \
    vim \
    zip \
    libtidy libtidy-devel \
    autoconf \
    gd gd-devel \
    patch \
    db4* \
    t1lib* t1lib-devel \
    openssl openssl-devel \
    bzip2 bzip2-devel \
    libcurl libcurl-devel \
    libxml2 libxml2-devel \
    libpng libpng-devel \
    libXpm libXpm-devel \
    libjpeg libjpeg-devel \
    iconv libiconv

RUN rpm --rebuilddb && yum install -y httpd httpd-devel

RUN rpm --rebuilddb && yum install -y \
  mysql-devel \
  openldap-devel \
  freetype-devel \
  gmp-devel \
  libmhash-devel \
  readline-devel \
  net-snmp-devel \
  libxslt-devel \
  libtool-ltdl-devel \
  libc-client-devel \
  ncurses-devel \
  postgresql-devel \
  aspell-devel \
  pcre-devel

WORKDIR /usr/local/src
RUN wget http://museum.php.net/php5/php-${PHP_VERSION}.tar.bz2
RUN tar -xf ./php-${PHP_VERSION}.tar.bz2 -C ./
WORKDIR /usr/local/src/php-${PHP_VERSION}
 
RUN ./configure \
      --prefix=/usr \
      --with-libdir=lib64 \
      --with-config-file-path=/etc \
      --with-config-file-scan-dir=/etc/php/conf.d \
      --bindir=/usr/bin \
      --sbindir=/usr/sbin \
      --sysconfdir=/etc \
      --enable-gd-native-ttf \
      --enable-mbregex \
      --enable-mbstring \
      --enable-zip \
      --enable-bcmath \
      --enable-soap \
      --enable-sockets \
      --enable-ftp \
      --with-apxs2 \
      --with-openssl \
      --with-zlib \
      --with-bz2 \
      --with-gettext \
      --with-iconv \
      --with-curl \
      --with-mysql-sock \
      --with-gd \
      --with-pdo-mysql \
      --with-pdo-pgsql \
      --with-xsl \
      --with-mysql \
      --with-mysqli \
      --with-freetype-dir \
      --with-jpeg-dir \
      --with-png-dir \
      --with-gmp \
      --with-pcre-regex \
      && make && make install \
      && cp -f ./php.ini-recommended /etc/php.ini \  
      && sed -i 's/^extension_dir/;extension_dir/g' /etc/php.ini \
      && sed -i 's/^LoadModule php5_module/#LoadModule php5_module/g' /etc/httpd/conf/httpd.conf \
    && mkdir -p /etc/php/conf.d \
      && rm -rf /usr/local/src/php*  

RUN yum clean all \
    && rm -rf /var/cache/yum \
    && rm -rf /var/tmp/*

COPY info.php /var/www/html/index.php

EXPOSE 8080

COPY ./httpd.conf /etc/httpd/conf/extra.conf
RUN echo 'Include /etc/httpd/conf/extra.conf' >> /etc/httpd/conf/httpd.conf
RUN echo 'ServerName localhost' >> /etc/httpd/conf/httpd.conf

# web server start
CMD [ "/usr/sbin/httpd", "-D", "FOREGROUND" ]
